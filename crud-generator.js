const Boom = require( 'boom' );
const mongoose = require( 'mongoose' );
const dbManifest = require( './config/database' );
const config = require( './config/config' );

function generateCRUDRoutes ( model ) {
	const modelName = model.name.toLowerCase();

	return [
		{
			'method' : 'GET',
			'path' : '/' +  modelName,
			'config' : {
				handler : function ( request, reply ) {
					var Model  = request.server.plugins[ 'crud-generator' ][ 'models' ][ modelName ];
					var search = Model.find( function ( err, searchResults ) {
						if ( err ) {
							console.log( err );
							return reply( Boom.badRequest( 'Something went wrong.', err ) );
						}

						return reply( searchResults );
					} );
				},
				auth : model.auth
			}
		},
		{
			'method' : 'GET',
			'path' : '/' +  modelName + '/{id}',
			'config' : {
				handler : function ( request, reply ) {
					var Model  = request.server.plugins[ 'crud-generator' ][ 'models' ][ modelName ];
					var search = Model.findById( request.params.id, function ( err, searchResults ) {
						if ( err ) {
							return reply( Boom.badRequest( 'Something went wrong.', err ) );
						}

						return reply( searchResults );
					} );
				},
				auth : model.auth
			}
		},
		{
			'method' : 'POST',
			'path' : '/' +  modelName,
			'config' : {
				handler : function ( request, reply ) {
					var Model  = request.server.plugins[ 'crud-generator' ][ 'models' ][ modelName ];
					var obj = new Model( request.payload );

					obj.save( function ( err, saved ) {
						if ( err ) {
							return reply( Boom.badRequest( 'Something went wrong.', err ) );
						}

						return reply( saved );
					} );
				},
				validate : model.validation,
				auth : model.auth
			}
		},
		{
			'method' : 'PUT',
			'path' : '/' +  modelName + '/{id}',
			'config' : {
				handler : function ( request, reply ) {
					var Model  = request.server.plugins[ 'crud-generator' ][ 'models' ][ modelName ];

					Model.update( { _id : request.params.id }, { $set : request.payload }, function ( err ) {
						if ( err ) {
							return reply( Boom.badRequest( 'Something went wrong.', err ) );
						}

						return reply( { 'status' : 'Data updated.' } );
					} );
				},
				validate : model.validation,
				auth : model.auth
			}
		},
		{
			'method' : 'DELETE',
			'path' : '/' +  modelName + '/{id}',
			'config' : {
				handler : function ( request, reply ) {
					var Model  = request.server.plugins[ 'crud-generator' ][ 'models' ][ modelName ];

					Model.remove( { _id : request.params.id }, function ( err ) {
						if ( err ) {
							return reply( Boom.badRequest( 'Something went wrong.', err ) );
						}

						return reply( { 'status' : 'Data removed.' } );
					} );

				},
				auth : model.auth

			}
		},
		{
			'method' : 'DELETE',
			'path' : '/' +  modelName + '/{id}/soft',
			'config' : {
				handler : function ( request, reply ) {
					var Model  = request.server.plugins[ 'crud-generator' ][ 'models' ][ modelName ];

					Model.update( { _id : request.params.id }, { $set : { 'active' : false } }, function ( err ) {
						if ( err ) {
							return reply( Boom.badRequest( 'Something went wrong.', err ) );
						}

						return reply( { 'status' : 'Data updated.' } );
					} );
				},
				auth : model.auth
			}
		}
	];
}

exports.register = function ( plugin, option, next ) {
	mongoose.Promise = global.Promise;
	mongoose.connect( config.database.url );

	var db = mongoose.connection;

	db.on( 'connected', function () {
		plugin.expose( 'mongoose', mongoose );
		plugin.expose( 'db', db );

		var models =  {};

		if ( dbManifest.models ) {
			dbManifest.models.forEach( function ( model ) {
				if ( model.save_hook || model.update_hook ) {
					model.schema = new mongoose.Schema( model.schema );

					if ( model.save_hook ) {
						model.schema.pre( 'save', model.save_hook );
					}

					if ( model.update_hook ) {
						model.schema.pre( 'update', model.update_hook );
					}
				}

				var Model = mongoose.model( model.name, model.schema );

				models[ model.name.toLowerCase() ] = Model;

				var routes = generateCRUDRoutes( model );

				plugin.route( routes );
			} );
		}

		/*
		 * Expose model as plugin
		 */
		plugin.expose( 'models', models );
	} );

	next();
};

exports.register.attributes = {
	'name' : 'crud-generator'
};
