var expect = require( 'chai' ).expect;
var request = require( 'request' );

var createdUser = {};

describe( 'User Module', function () {
	describe( 'POST /user', function () {
		it( 'should be able to create a new user', function ( done ) {
			request( {
				'url' : 'http://localhost:8080/api/user',
				'json' : true,
				'method' : 'post',
				'body' : {
					// try to generate a random username
					'username' : 'user' + ( new Date() ).getTime(),
					'password' : 'admin'
				}
			}, function ( error, response, body ) {
				expect( response.statusCode ).to.equal( 200 );
				expect( body._id ).to.not.equal( undefined );

				createdUser = body;

				done();
			} );
		} );
	} );

	describe( 'PUT /user', function () {
		it( 'should be able to update the user we previously created', function ( done ) {
			request( {
				'url' : `http://localhost:8080/api/user/${ createdUser._id}`,
				'json' : true,
				'method' : 'put',
				'body' : {
					'username' : 'something',
					'password' : 'testpassword'
				},
			}, function ( error, response, body ) {
				expect( response.statusCode ).to.equal( 200 );
				expect( body.status ).to.equal( 'Data updated.' );

				done();
			} );
		} );
	} )

	describe( 'GET /user', function () {
		it( 'should be able to retrieve user', function ( done ) {
			request( {
				'url' : 'http://localhost:8080/api/user',
				'json' : true
			}, function ( error, response, body ) {
				expect( response.statusCode ).to.equal( 200 );
				expect( body.length ).to.not.equal( 0 );

				done();
			} );
		} );
	} );
} );
