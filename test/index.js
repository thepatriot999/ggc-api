const fs = require( 'fs' );
const expect = require( 'chai' ).expect;
const Glue = require( 'glue' );
const manifest = require( '../config/manifest.js' );

/*
 * Flag if the server has successfully started.
 */
var started = false;

before( function () {
	Glue.compose( manifest, {
		relativeTo : __dirname + '/../'
	}, function ( err, server ) {
		if ( err ) {
			return console.log( err );
		}

		server.start( function () {
			started = true;

			server.on( 'db_connected', function () {
				console.log( 'Database connected.' );
			} );
		} );
	} );
} );

describe( 'API Server', function () {
	it( 'status check', function () {
		expect( started ).to.equal( true );

		var dir = __dirname + '/modules/';

		fs.readdir( dir, function ( err, files ) {
			if ( err ) {
				return console.log( err );
			}

			files.map( function ( file ) {
				return dir + file;
			} ).forEach( require );
		} );
	} );
} );



