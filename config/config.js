module.exports = {
	'database' : {
		'url' : process.env.DATABASE_URI || 'mongodb://192.168.99.100:27017/ggc'
	}, 
	'auth_key' : process.env.AUTH_KEY || 'some_secret_secret',
	'password_hash' : process.env.PASSWORD_HASH || '$2a$10$l54tPTt6hynAiT0nvTogou'
};
