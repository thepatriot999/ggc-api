const manifest = {
	connections : [
		{
			port   : process.env.PORT || 8080,
			labels : [ 'dev' ],
			routes : {
				cors : true
			}
		}
	],
	registrations : [
		{
			plugin : 'hapi-auth-jwt2',
		},
		{
			plugin  : './auth',
			options : {
				routes : {
					prefix: '/api'
				}
			}
		},
		{
			plugin : './crud-generator',
			options : {
				routes : {
					prefix : '/api'
				}
			}
		},
		{
			plugin  : './api',
			options : {
				routes : {
					prefix : '/api'
				}
			}
		}
	]
};

module.exports = manifest;
