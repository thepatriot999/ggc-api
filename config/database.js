const Joi = require( 'joi' );
var save_hook = require( './database/user_hook' ).save_hook;
var update_hook = require( './database/user_hook' ).update_hook;

const database = {
	models : [
		{
			'name'       : 'Post',
			'schema'     : {
				'content'    : String,
				'user_id'    : String,
				'date_added' : Date
			},
			'validation' : {
				'payload' : {
					content    : Joi.string().required(),
					user_id    : Joi.string().required(),
					date_added : Joi.date().required()
				}
			},
			'auth' : 'jwt'
		},
		{
			'name'   : 'User',
			'schema' : {
				'username' : String,
				'password' : String,
				'is_admin' : Number,
				'added'    : Date
			},
			'validation' : {
				'payload' : {
					username : Joi.string().required(),
					password : Joi.string().required(),
					is_admin : Joi.number().optional()
				}
			},
			'auth' : null,
			'save_hook'   : save_hook,
			'update_hook' : update_hook
		}
	]
};

module.exports = database;
