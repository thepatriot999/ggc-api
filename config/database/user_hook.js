var bcrypt = require( 'bcrypt' );
var config = require( '../config' );

exports.save_hook = function saveHook( next ) {
	var obj = this;

	bcrypt.hash( this.password, config.password_hash, function ( err, hash ) {
		if ( err ) {
			return next( err );
		}

		obj.password = hash;

		next();
	} );
}

exports.update_hook = function updateHook( next ) {
	var obj = this._update[ '$set' ];

	bcrypt.hash( obj.password, config.password_hash, function ( err, hash ) {
		if ( err ) {
			return next( err );
		}

		obj.password = hash;

		next();
	} );
}
