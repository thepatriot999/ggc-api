const Boom = require( 'boom' );
const bcrypt = require( 'bcrypt' );
const config = require( './config/config' );
const jwt = require( 'jsonwebtoken' );

exports.register = function ( plugin, options, next ) {
	plugin.auth.strategy( 'jwt', 'jwt', {
		'key' 		: config.auth_key,
		'verifyOptions' : {
			'algorithms' : [ 'HS256' ]
		},
		'validateFunc' : function ( decoded, request, callback ) {
			var Model  = request.server.plugins[ 'crud-generator' ][ 'models' ][ 'user' ];
			var search = Model.findOne( {
				'_id' : decoded.id
			}, function ( err, searchResults ) {
				if ( err || !searchResults ) {
					return callback( null, false );
				}

				return callback( null, true );
			} );
		}
	} );

	plugin.route( {
		'method' : 'POST',
		'path'	 : '/authenticate',
		'config' : {
			handler : function ( req, reply ) {
				var payload = req.payload;

				bcrypt.hash( payload.password, config.password_hash, function ( err, hashedPassword ) {
					if ( err ) {
						return reply( Boom.badRequest( 'Something went wrong.' ) );
					}

					var Model  = req.server.plugins[ 'crud-generator' ][ 'models' ][ 'user' ];
					var search = Model.findOne( {
						'username' : payload.username,
						'password' : hashedPassword
					}, function ( err, user ) {
						if ( err || !user ) {
							return reply( Boom.badRequest( 'Invalid credentials.' ) );
						}

						/*
						 * Copy username and id to an object ang sign jwt
						 */
						var signObject = {
							'username' : user.username,
							'id'       : user._id
						};

						var token = jwt.sign( signObject, process.env.AUTH_KEY || config.auth_key );

						reply( { 'token' : token } );
					} );
				} );

			}
		}
	} );

	next();
};

exports.register.attributes = {
	'name' : 'auth'
};
